﻿using ForecastIO;
using System.Collections.Generic;
using System.ComponentModel;

namespace Arcules
{
    public class ForecastViewModel : INotifyPropertyChanged
    {
        public ForecastViewModel()
        {

        }

        private float oldLatitude = 0;
        private float oldLongitude = 0;
        private float latitude = 0;
        private float longitude = 0;

        public float Latitude
        {
            get
            {
                return latitude;
            }
            set
            {
                latitude = value;
                RaisePropertyChanged("Longitude");
                RaisePropertyChanged("Forecasts");
            }
        }
        public float Longitude
        {
            get
            {
                return longitude;
            }
            set
            {
                longitude = value;
                RaisePropertyChanged("Longitude");
                RaisePropertyChanged("Forecasts");
            }
        }

        private List<DailyForecast> forecasts;
        public List<DailyForecast> Forecasts
        {
            get
            {
                if (latitude != 0 && longitude != 0 
                    && (oldLatitude != latitude || oldLongitude != longitude))
                {
                    GetData();
                    oldLatitude = latitude;
                    oldLongitude = latitude;
                    return forecasts;
                }

                return new List<DailyForecast>();
            }
            set
            {
                forecasts = value;
                RaisePropertyChanged("Forecasts");
            }
        }

        private void GetData()
        {
            var request = new ForecastIORequest("052d82150c05ed6f778b352e6a182876", latitude, longitude, Unit.si);
            forecasts = request.Get().daily.data;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
