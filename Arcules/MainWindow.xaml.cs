﻿using ForecastIO;
using System.Windows;

namespace Arcules
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            DataContext = new ForecastViewModel();
            InitializeComponent();
        }
    }
}
